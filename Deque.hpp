// -------
// Deque.h
// --------

#ifndef Deque_h
#define Deque_h

// --------
// includes
// --------

#include <algorithm>        // copy, equal, lexicographical_compare, max, swap
#include <cassert>          // assert
#include <initializer_list> // initializer_list
#include <iterator>         // bidirectional_iterator_tag
#include <memory>           // allocator
#include <stdexcept>        // out_of_range
#include <utility>          // !=, <=, >, >=
#include <deque>            // TESTING

// -----
// using
// -----

using std::rel_ops::operator!=;
using std::rel_ops::operator<=;
// -------
using std::rel_ops::operator>;
using std::rel_ops::operator>=;

// destroy
// -------

template <typename A, typename BI>
BI my_destroy (A& a, BI b, BI e) {
    while (b != e) {
        --e;
        a.destroy(&*e);
    }
    return b;
}

// ------------------
// uninitialized_copy
// ------------------

template <typename A, typename II, typename BI>
BI my_uninitialized_copy (A& a, II b, II e, BI x) {
    BI p = x;
    try {
        while (b != e) {
            a.construct(&*x, *b);
            ++b;
            ++x;
        }
    }
    catch (...) {
        my_destroy(a, p, x);
        throw;
    }
    return x;
}

// ------------------
// uninitialized_fill
// ------------------

template <typename A, typename BI, typename T>
void my_uninitialized_fill (A& a, BI b, BI e, const T& v) {
    BI p = b;
    try {
        while (b != e) {
            a.construct(&*b, v);
            ++b;
        }
    }
    catch (...) {
        my_destroy(a, p, b);
        throw;
    }
}

// --------
// my_deque
// --------

template <typename T, typename A = std::allocator<T>>
class my_deque {
    // -----------
    // operator ==
    // -----------

    /**
     * @param lhs   deque on the left hand side of == operator
     * @param rhs   deque on the right hand side of == operator
     * @return      a bool indicating whether the the params have equivalent elements
     */
    friend bool operator == (const my_deque& lhs, const my_deque& rhs) {
        // you must use std::equal()
        return std::equal(std::begin(lhs), std::end(lhs), std::begin(rhs), std::end(rhs));
    }

    // ----------
    // operator <
    // ----------

    /**
     * @param lhs   my_deque on the left hand side of == operator
     * @param rhs   my_deque on the right hand side of == operator
     * @return      a bool indicating which container has lexicographically greater elements beginning
     *              from the first of each container
     */
    friend bool operator < (const my_deque& lhs, const my_deque& rhs) {
        // you must use std::lexicographical_compare()
        return std::lexicographical_compare(std::begin(lhs), std::end(lhs), std::begin(rhs), std::end(rhs));
    }

    // ----
    // swap
    // ----

    /**
     * @param x   my_deque
     * @param y   my_deque
     * swap contents of deque x and deque y
     */
    friend void swap (my_deque& x, my_deque& y) {
        x.swap(y);
    }

public:
    // ------
    // usings
    // ------

    // you must use this allocator for the inner arrays
    using allocator_type  = A;
    using value_type      = typename allocator_type::value_type;

    using size_type       = typename allocator_type::size_type;
    using difference_type = typename allocator_type::difference_type;

    using pointer         = typename allocator_type::pointer;
    using const_pointer   = typename allocator_type::const_pointer;

    using reference       = typename allocator_type::reference;
    using const_reference = typename allocator_type::const_reference;

    // you must use this allocator for the outer array
    using allocator_type_2 = typename A::template rebind<pointer>::other;

private:
    // ----
    // data
    // ----

    /**
     * Arbitrary bucket size for the inner array
     */
    static constexpr size_t kBucketSize = 256;

    /**
     * Allocator for the actual objects
     */
    allocator_type a_;

    /**
     * Allocator for the outer array of arrays
     */
    allocator_type_2 oa_;

    /**
     * Pointer to the beginning of the outer array which stores arrays of T
     */
    T** base_;

    /**
     * Current number of buckets in the data outer array
     */
    size_t nbuckets_;

    /**
     * Index representing begin/end of segment that actually contains data.
     */
    size_t b_, e_;

    // <your data>
private:
    // -----
    // valid
    // -----

    /**
     * @return Whether the deque is in a valid state or not
     */
    bool valid () const {
        if (e_ < b_) return false;
        if (b_ >= nbuckets_ * kBucketSize || e_ >= nbuckets_ * kBucketSize) return false;
        return true;
    }

public:
    // --------
    // iterator
    // --------

    class iterator {
        // -----------
        // operator ==
        // -----------

        /**
         * @param lhs       iterator on left hand side of == operator
         * @param rhs       iterator on right hand side of == operator
         * @return          a bool indicating whether the iterator refers to the same my_deque
         *                  object and whether they refer to the same element in that obejct
         */
        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return (lhs.deque_ == rhs.deque_) && (lhs.index_ == rhs.index_);
        }

        /**
        * @param lhs       iterator on left hand side of == operator
         * @param rhs       iterator on right hand side of == operator
         * @return          a bool indicating whether the iterator refers to a different my_deque
         *                  object or whether they refer to a different element in the same object
         */
        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

        // ----------
        // operator +
        // ----------

        /**
         * @param lhs       a my_deque iterator
         * @param rhs       a numeral indicating how how much to increase the index of this iterator
         * @return          an iterator seeked ahead
         */
        friend iterator operator + (iterator lhs, difference_type rhs) {
            return lhs += rhs;
        }

        // ----------
        // operator -
        // ----------

        /**
         * @param lhs       a my_deque iterator
         * @param rhs       a numeral indicating how how much to decrease the index of this iterator
         * @return          an iterator seeked backwards
         */
        friend iterator operator - (iterator lhs, difference_type rhs) {
            return lhs -= rhs;
        }

    public:
        // ------
        // usings
        // ------

        // this requires a weaker iterator than the real deque provides
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type        = typename my_deque::value_type;
        using difference_type   = typename my_deque::difference_type;
        using pointer           = typename my_deque::pointer;
        using reference         = typename my_deque::reference;

    private:
        // ----
        // data
        // ----

        // <your data>

        /**
         * pointer to the instance of my_deque this iterator came from
         */
        my_deque<T, A>* deque_;

        /**
         * Index of the current element in the deque this iterator points to
         */
        size_t index_ = 0;

    private:
        // -----
        // valid
        // -----

        /**
         * Returns whether the iterator is in a valid state
         */
        bool valid () const {
            if (deque_ == nullptr) return false;
            return true;
        }

    public:
        // -----------
        // constructor
        // -----------

        /**
         * your documentation
         * Use a pointer instead of a reference because we want to be able to check if
         * Two iterators came from the same deque
         */
        iterator (my_deque<T, A>* dq, size_t idx = 0) : deque_(dq), index_(idx) {
            assert(valid());
        }
        iterator             ()                = default;
        iterator             (const iterator&) = default;
        ~iterator            ()                = default;
        iterator& operator = (const iterator&) = default;

        // ----------
        // operator *
        // ----------

        /**
         * @return      a reference to an element T of this deque
         */
        reference operator * () const {
            assert(valid());
            return (*deque_)[index_];
        }           // fix

        // -----------
        // operator ->
        // -----------

        /**
         @return        a pointer to an element T of this deque
         */
        pointer operator -> () const {
            assert(valid());
            return &(*deque_)[index_];
        }

        // -----------
        // operator ++
        // -----------

        /**
         *              increment this iterator
         * @return      a reference to this incremented iterator
         */
        iterator& operator ++ () {
            assert(valid());

            ++index_;

            assert(valid());
            return *this;
        }

        /**
         *              increment this iterator
         @return        a copy of this iterator representing its state before increment
         */
        iterator operator ++ (int) {
            assert(valid());

            iterator x = *this;
            ++(*this);

            assert(valid());
            return x;
        }

        // -----------
        // operator --
        // -----------

        /**
         *             decrement this iterator
         * @return     a reference to this decremented iterator
         * your documentation
         */
        iterator& operator -- () {
            assert(valid());

            --index_;

            assert(valid());
            return *this;
        }

        /**
         *              decrement this iterator
         * @return      a copy of this iterator representing its state before decrement
         */
        iterator operator -- (int) {
            assert(valid());

            iterator x = *this;
            --(*this);

            assert(valid());
            return x;
        }

        // -----------
        // operator +=
        // -----------

        /**
         *              seek this iterator ahead by d
         * @return      a reference to this iterator
         */
        iterator& operator += (difference_type d) {
            assert(valid());

            index_ += d;

            assert(valid());
            return *this;
        }

        // -----------
        // operator -=
        // -----------

        /**
         *              rewind this iterator by d
         * @return      a reference to this iterator
         */
        iterator& operator -= (difference_type d) {
            assert(valid());

            index_ -= d;

            assert(valid());
            return *this;
        }
    }; // iterator

public:
    // --------------
    // const_iterator
    // --------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        /**
         * @param lhs       iterator on left hand side of == operator
         * @param rhs       iterator on right hand side of == operator
         * @return          a bool indicating whether the iterator refers to the same my_deque
         *                  object and whether they refer to the same element in that obejct
         */
        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return (lhs.deque_ == rhs.deque_) && (lhs.index_ == rhs.index_);
        } // fix

        /**
        * @param lhs       iterator on left hand side of == operator
        * @param rhs       iterator on right hand side of == operator
        * @return          a bool indicating whether the iterator refers to a different my_deque
        *                  object or whether they refer to a different element in the same object
        */
        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

        // ----------
        // operator +
        // ----------

        /**
         * @param lhs       a my_deque iterator
         * @param rhs       a numeral indicating how how much to increase the index of this iterator
         * @return          an iterator seeked ahead
         */
        friend const_iterator operator + (const_iterator lhs, difference_type rhs) {
            return lhs += rhs;
        }

        // ----------
        // operator -
        // ----------

        /**
         * @param lhs       a my_deque iterator
         * @param rhs       a numeral indicating how how much to decrease the index of this iterator
         * @return          an iterator seeked backwards
         */
        friend const_iterator operator - (const_iterator lhs, difference_type rhs) {
            return lhs -= rhs;
        }

    public:
        // ------
        // usings
        // ------

        // this requires a weaker iterator than the real deque provides
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type        = typename my_deque::value_type;
        using difference_type   = typename my_deque::difference_type;
        using pointer           = typename my_deque::const_pointer;
        using reference         = typename my_deque::const_reference;

    private:
        // ----
        // data
        // ----

        // <your data>
        /**
         * A pointer to the instance of my_deque this iterator came from
         */
        const my_deque<T, A>* deque_;

        /**
         * Represents the index of the element this iterator is currently pointing at
         */
        size_t index_ = 0;

    private:
        // -----
        // valid
        // -----

        /**
         * Checks whether this iterator is currently in a valid state or not
         */
        bool valid () const {
            if (deque_ == nullptr) return false;
            return true;
        }

    public:
        // -----------
        // constructor
        // -----------

        /**
         * your documentation
         */
        const_iterator (const my_deque<T, A>* dq, size_t idx = 0) : deque_(dq), index_(idx) {
            assert(valid());
        }

        const_iterator             ()                      = default;
        const_iterator             (const const_iterator&) = default;
        ~const_iterator            ()                      = default;
        const_iterator& operator = (const const_iterator&) = default;

        // ----------
        // operator *
        // ----------

        /**
         * @return      a const_reference to an element T of this deque
         */
        reference operator * () const {
            assert(valid());
            return (*deque_)[index_];
        }           // fix

        // -----------
        // operator ->
        // -----------

        /**
         @return        a const_pointer to an element T of this deque
         */
        pointer operator -> () const {
            assert(valid());
            return &(*deque_)[index_];
        }

        // -----------
        // operator ++
        // -----------

        /**
         *              increment this const_iterator
         * @return      a reference to this incremented const_iterator
         */
        const_iterator& operator ++ () {
            assert(valid());

            ++index_;

            assert(valid());
            return *this;
        }

        /**
         *              increment this const_iterator
         @return        a copy of this const_iterator representing its state before increment
         */
        const_iterator operator ++ (int) {
            assert(valid());

            const_iterator x = *this;
            ++(*this);

            assert(valid());
            return x;
        }

        // -----------
        // operator --
        // -----------

        /**
        *             decrement this iterator
        * @return     a reference to this decremented const_iterator
        * your documentation
        */
        const_iterator& operator -- () {
            assert(valid());

            --index_;

            assert(valid());
            return *this;
        }

        /**
        *              decrement this const_iterator
        * @return      a copy of this const_iterator representing its state before decrement
        */
        const_iterator operator -- (int) {
            assert(valid());

            const_iterator x = *this;
            --(*this);

            assert(valid());
            return x;
        }

        // -----------
        // operator +=
        // -----------

        /**
         *              seek this const_iterator ahead by d
         * @return      a reference to this iterator
         */
        const_iterator& operator += (difference_type d) {
            assert(valid());

            index_ += d;

            assert(valid());
            return *this;
        }

        // -----------
        // operator -=
        // -----------

        /**
         *              rewind this const_iterator by d
         * @return      a reference to this iterator
         */
        const_iterator& operator -= (difference_type d) {
            assert(valid());

            index_ -= d;

            assert(valid());
            return *this;
        }
    }; // const_iterator

public:
    // ------------
    // constructors
    // ------------

    my_deque () : a_(), oa_(), nbuckets_(1) {
        base_ = oa_.allocate(nbuckets_);
        base_[0] = a_.allocate(kBucketSize);
        b_ = nbuckets_ * kBucketSize / 4;
        e_ = b_;

        assert(valid());
    }

    /**
     *              constructor
     * @param s     initial size argument
     */
    explicit my_deque (size_type s) : a_(), oa_(), nbuckets_(2 * (s / kBucketSize) + 1) {
        // <your code>
        base_ = oa_.allocate(nbuckets_);
        for (int i = 0; i < (int) nbuckets_; ++i) {
            base_[i] = a_.allocate(kBucketSize);
        }
        b_ = nbuckets_ * kBucketSize / 4;
        e_ = b_ + s;

        assert(valid());
    }

    /**
     *              constructor
     * @param s     initial size argument
     * @param v     initial element to fill
     */
    my_deque (size_type s, const_reference v) : a_(), oa_(), nbuckets_(2 * (s / kBucketSize) + 1) {
        // <your code>
        base_ = oa_.allocate(nbuckets_);
        for (int i = 0; i < (int) nbuckets_; ++i) {
            base_[i] = a_.allocate(kBucketSize);
        }
        b_ = nbuckets_ * kBucketSize / 4;
        e_ = b_ + s;
        my_uninitialized_fill(a_, begin(), end(), v);
        assert(valid());
    }


    /**
     *              constructor
     * @param s     initial size argument
     * @param v     initial element to fill
     * @param a     custom allocator
     */
    my_deque (size_type s, const_reference v, const allocator_type& a) : a_(a), oa_(), nbuckets_(2 * (s / kBucketSize) + 1) {
        // <your code>
        base_ = oa_.allocate(nbuckets_);
        for (int i = 0; i < (int) nbuckets_; ++i) {
            base_[i] = a_.allocate(kBucketSize);
        }
        b_ = nbuckets_ * kBucketSize / 4;
        e_ = b_ + s;
        my_uninitialized_fill(a_, begin(), end(), v);
        assert(valid());
    }

    /**
     *              constructor
     * @param rhs   initializer list
     */
    my_deque (std::initializer_list<value_type> rhs) : a_(), oa_(), nbuckets_(2 * (std::distance(std::begin(rhs), std::end(rhs)) / kBucketSize) + 1) {
        // <your code>
        base_ = oa_.allocate(nbuckets_);
        for (int i = 0; i < (int) nbuckets_; ++i) {
            base_[i] = a_.allocate(kBucketSize);
        }
        b_ = nbuckets_ * kBucketSize / 4;
        e_ = b_ + std::distance(std::begin(rhs), std::end(rhs));
        my_uninitialized_copy(a_, std::begin(rhs), std::end(rhs), begin());
        assert(valid());
    }

    /**
     *              constructor
     * @param rhs   initializer list
     * @param a     custom allocator
     */
    my_deque (std::initializer_list<value_type> rhs, const allocator_type& a) : a_(a), oa_(), nbuckets_(2 * (std::distance(std::begin(rhs), std::end(rhs)) / kBucketSize) + 1) {
        // <your code>
        base_ = oa_.allocate(nbuckets_);
        for (int i = 0; i < (int) nbuckets_; ++i) {
            base_[i] = a_.allocate(kBucketSize);
        }
        b_ = nbuckets_ * kBucketSize / 4;
        e_ = b_ + std::distance(std::begin(rhs), std::end(rhs));
        my_uninitialized_copy(a_, std::begin(rhs), std::end(rhs), begin());
        assert(valid());
    }

    /**
    *              copy constructor
    * @param that   my_deque to copy
    */
    my_deque (const my_deque& that) {
        // <your code>
        nbuckets_ = that.nbuckets_;
        base_ = oa_.allocate(nbuckets_);
        for (int i = 0; i < (int) nbuckets_; ++i) {
            base_[i] = a_.allocate(kBucketSize);
        }
        b_ = that.b_;
        e_ = that.e_;
        my_uninitialized_copy(a_, std::begin(that), std::end(that), begin());
        assert(valid());
    }

    // ----------
    // destructor
    // ----------

    /**
     * destructor
     * free up memory
     */
    ~my_deque () {
        // <your code>
        destroy_all();
        assert(valid());
    }

    // ----------
    // operator =
    // ----------
    /**
    * destructor
    * free up memory
    */
    void destroy_all() {
        my_destroy(a_, begin(), end());
        for (int i = 0; i < (int) nbuckets_; ++i) {
            a_.deallocate(base_[i], kBucketSize);
        }
        oa_.deallocate(base_, nbuckets_);
    }
    /**
     *                  copy constructor
     * @param rhs       read-only reference to a my_deque
     */
    my_deque& operator = (const my_deque& rhs) {
        // <your code>
        destroy_all();
        a_ = rhs.a_;
        oa_ = rhs.oa_;
        nbuckets_ = rhs.nbuckets_;
        base_ = oa_.allocate(nbuckets_);
        for (int i = 0; i < (int) nbuckets_; ++i) {
            base_[i] = a_.allocate(kBucketSize);
        }
        b_ = rhs.b_;
        e_ = rhs.e_;
        my_uninitialized_copy(a_, std::begin(rhs), std::end(rhs), begin());
        assert(valid());
        return *this;
    }

    // -----------
    // operator []
    // -----------

    /**
     * @param index     index
     * @return          a reference to the element at index
     */
    reference operator [] (size_type index) {
        // <your code>
        index += b_;
        return base_[index / kBucketSize][index % kBucketSize];
    }           // fix

    /**
     * @param index     index
     * @return          a read only reference to the element at index
     */
    const_reference operator [] (size_type index) const {
        return const_cast<my_deque*>(this)->operator[](index);
    }

    // --
    // at
    // --

    /**
     * @param index     index
     * @return          a reference to the element in my_deque at index
     * @throws out_of_range
     */
    reference at (size_type index) {
        if (index < 0 || index >= size()) {
            throw std::out_of_range("Index out of range of dexque");
        }
        return (*this)[index];
    }           // fix

    /**
    * @param index     index
    * @return          a const_reference to the element in my_deque at index
    * @throws out_of_range
    */
    const_reference at (size_type index) const {
        return const_cast<my_deque*>(this)->at(index);
    }

    // ----
    // back
    // ----

    /**
    * @return          a reference to the element in my_deque at the back
    * @throws out_of_range
    */
    reference back () {
        return at(size() - 1);
    }           // fix

    /**
     * @return          a const_reference to the element in my_deque at the back
     * @throws out_of_range
     */
    const_reference back () const {
        return const_cast<my_deque*>(this)->back();
    }

    // -----
    // begin
    // -----

    /**
     * @return an iterator pointing to the start of this my_deque
     */
    iterator begin () {
        return iterator(this);
    }

    /**
     * @return an const_iterator pointing to the start of this my_deque
     */
    const_iterator begin () const {
        return const_iterator(this);
    }

    // -----
    // clear
    // -----

    /**
     * remove all elements
     */
    void clear () {
        // <your code>
        *this = my_deque();
        assert(valid());
    }

    // -----
    // empty
    // -----

    /**
     * @return      a bool indicating whether the my_deque is empty
     */
    bool empty () const {
        return !size();
    }

    // ---
    // end
    // ---

    /**
     @return        an iterator pointing to the end of this my_deque
     */
    iterator end () {
        return iterator(this, size());
    }

    /**
     an const_iterator pointing to the end of this my_deque
     */
    const_iterator end () const {
        return const_iterator(this, size());
    }

    // -----
    // erase
    // -----

    /**
     *                  erase the element at index i
     * @param it        my_deque iterator
     * @return          iterator presenting the front of the new my_deque
     */
    iterator erase (iterator it) {
        // <your code>
        if (it == end()) return end();

        difference_type idx = std::distance(begin(), it);
        bool towards_front = (idx <= std::distance(it, end()));

        if (towards_front) {
            // rotates so that it is the first element now
            std::rotate(begin(), it, it + 1);
            // delete front element
            a_.destroy(&*begin());
            ++b_;
        } else {
            // rotates so that the it + 1 is where it used to be and it is at the end
            std::rotate(it, it + 1, end());
            a_.destroy(&*(--end()));
            --e_;
            // delete back element
        }
        assert(valid());
        return begin();
    }

    // -----
    // front
    // -----

    /**
     * @return      reference to element at front of deque (0th index)
     */
    reference front () {
        return at(0);
    }           // fix

    /**
     * @return  return const_reference to element at front of deque (0th index)
     */
    const_reference front () const {
        return const_cast<my_deque*>(this)->front();
    }

    // ------
    // insert
    // ------

    /**
     *              insert an element into the deque
     * @return      an iterator referring to position of element that was added
     */
    iterator insert (iterator it, const_reference v) {
        // <your code>
        difference_type idx = std::distance(begin(), it);
        bool towards_front = (idx <= std::distance(it, end()));
        iterator bit;
        if (towards_front) {
            if (b_ == 0) reserve(2 * capacity());
            --b_;
            bit = begin();
            while (bit != it) {
                *bit = *(bit + 1);
                ++bit;
            }
            a_.construct(&*bit, v);
        } else {
            if (e_ == capacity()) reserve(2 * capacity());
            bit = end();
            ++e_;
            while (bit != it) {
                *bit = *(bit - 1);
                --bit;
            }
            a_.construct(&*bit, v);
        }
        assert(valid());
        return bit;
    }

    // ---
    // pop
    // ---

    /**
     * remove element at back
     */
    void pop_back () {
        auto e = --end();
        erase(e);
        assert(valid());
    }

    /**
     * remove element at front
     */
    void pop_front () {
        erase(begin());
        assert(valid());
    }

    // ----
    // push
    // ----

    /**
     * push an element to the back of the deque
     */
    void push_back (const_reference v) {
        insert(end(), v);
        assert(valid());
    }

    /**
     * push element to front of deque
     */
    void push_front (const_reference v) {
        insert(begin(), v);
        assert(valid());
    }

    // ------
    // resize
    // ------

    /**
     * wrapper for other resize
     */
    void resize (size_type s) {
        // <your code>
        resize(s, T());
        assert(valid());
    }

    /**
     *  resize the array to a new size at least s
     */
    void resize (size_type s, const_reference v) {
        // <your code>
        if (s == size()) return;
        else if (s < size()) {
            my_destroy(a_, begin() + s, end());
            e_ = b_ + s;
        } else {
            int num_to_add = s - size();
            if ((e_ + num_to_add) > capacity()) reserve(capacity() * 2);
            my_uninitialized_fill(a_, end(), end() + num_to_add, v);
            e_ += num_to_add;
        }
        assert(valid());
    }
    /**
     *  return capacity of deque, capacity > size
     */
    size_t capacity() {
        return nbuckets_ * kBucketSize;
    }

    void reserve(size_t s) {
        // <your code>
        my_deque other(s);
        // Only copy the pointer to the arrays not the objects
        my_uninitialized_copy(oa_, begin(), end(), other.begin());
        swap(other);
        assert(valid());
    }

    // ----
    // size
    // ----

    /**
     * @return      number of elements in my_deque
     */
    size_type size () const {
        return e_ - b_;
    }

    // ----
    // swap
    // ----

    /**
     *                  swaps this my_deque with other
     * @param other     other my_deque to swap with
     *
     */
    void swap (my_deque& other) {
        std::swap(base_, other.base_);
        std::swap(b_, other.b_);
        std::swap(e_, other.e_);
        std::swap(nbuckets_, other.nbuckets_);
        std::swap(oa_, other.oa_);
        std::swap(a_, other.a_);
        assert(valid());
    }
}; // my_deque

#endif // Deque_h
