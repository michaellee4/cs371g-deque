# CS371g: Generic Programming Deque Repo

* Name: (your Full Name)
- Michael Lee, Ewin Zuo

* EID: (your EID)
- ml45898, eztso

* GitLab ID: (your GitLab ID)
- michaellee4, eztso

* Git SHA: 0a98ea30709365b5d5ac2ca76768c54ea4738aa0 

* GitLab Pipelines: (link to your GitLab CI Pipeline)
- https://gitlab.com/michaellee4/cs371g-deque/-/pipelines

* Estimated completion time: 
- 10 hours 

* Actual completion time: (actual time in hours, int or float)
- 8 hours

* Comments: (any additional comments you have)
