// -------------
// TestDeque.c++
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// -----
// Using
// -----

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

using
deque_types =
    Types<
    deque<int>,
    my_deque<int>,
    deque<int, allocator<int>>,
    my_deque<int, allocator<int>>>;

#ifdef __APPLE__
TYPED_TEST_CASE(DequeFixture, deque_types,);
#else
TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

TYPED_TEST(DequeFixture, test1) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);
}

TYPED_TEST(DequeFixture, test2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test3) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x;
    const_iterator b = begin(x);
    const_iterator e = end(x);
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test_push_back1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(1);
    x.push_back(2);
    x.push_back(3);
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), 3u);
}

TYPED_TEST(DequeFixture, test_push_back2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(1);
    x.push_back(2);
    x.push_back(3);
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), 3u);
    ASSERT_EQ(x[0], 1);
    ASSERT_EQ(x[1], 2);
    ASSERT_EQ(x[2], 3);
}

TYPED_TEST(DequeFixture, test_push_front1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_front(1);
    x.push_front(2);
    x.push_front(3);
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), 3u);
}

TYPED_TEST(DequeFixture, test_push_front2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_front(1);
    x.push_front(2);
    x.push_front(3);
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), 3u);
    ASSERT_EQ(x[0], 3);
    ASSERT_EQ(x[1], 2);
    ASSERT_EQ(x[2], 1);
}

TYPED_TEST(DequeFixture, test_size_constructor) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(3);
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), 3u);
}

TYPED_TEST(DequeFixture, test_size_value_constructor) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(3, 3);
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), 3u);
    ASSERT_EQ(x[0], 3);
    ASSERT_EQ(x[1], 3);
    ASSERT_EQ(x[2], 3);
}

TYPED_TEST(DequeFixture, test_initializer_list_constructor) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x {1, 2, 3};
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), 3u);
    ASSERT_EQ(x[0], 1);
    ASSERT_EQ(x[1], 2);
    ASSERT_EQ(x[2], 3);
}

TYPED_TEST(DequeFixture, test_copy_constructor) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x {1, 2, 3};
    deque_type y (x);
    ASSERT_EQ(y[0], 1);
    ASSERT_EQ(y[1], 2);
    ASSERT_EQ(y[2], 3);
}

TYPED_TEST(DequeFixture, test_copy_assignment) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x {1, 2, 3};
    deque_type y = x;
    ASSERT_EQ(y[0], 1);
    ASSERT_EQ(y[1], 2);
    ASSERT_EQ(y[2], 3);
}

TYPED_TEST(DequeFixture, test_index_operator) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(1);
    x.push_back(2);
    x.push_back(3);
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), 3u);
    ASSERT_EQ(x[0], 1);
    ASSERT_EQ(x[1], 2);
    ASSERT_EQ(x[2], 3);
}

TYPED_TEST(DequeFixture, test_at) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(1);
    x.push_back(2);
    x.push_back(3);
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), 3u);
    ASSERT_EQ(x.at(0), 1);
    ASSERT_EQ(x.at(1), 2);
    ASSERT_EQ(x.at(2), 3);
}

TYPED_TEST(DequeFixture, test_at_throw) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    try {
        x.at(0);
    } catch (const std::exception& e) {
    }
}

TYPED_TEST(DequeFixture, test_back) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x {1, 2, 3};
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), 3u);
    ASSERT_EQ(x.back(), 3);
}

TYPED_TEST(DequeFixture, test_front) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x {1, 2, 3};
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), 3u);
    ASSERT_EQ(x.front(), 1);
}

TYPED_TEST(DequeFixture, test_iterator) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x {1, 2, 3};
    iterator b = begin(x);
    iterator e = end(x);
    EXPECT_TRUE(b != e);
    ASSERT_EQ(*b, 1);
    ++b;
    ASSERT_EQ(*b, 2);
    ++b;
    ASSERT_EQ(*b, 3);
    ++b;
    EXPECT_TRUE(b==e);
}

TYPED_TEST(DequeFixture, test_iterator_distance) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x {1, 2, 3};
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(std::distance(b, e), 3);
}

TYPED_TEST(DequeFixture, test_iterator_range_for) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x {1, 2, 3};
    int i = 1;
    for (int& j : x) {
        ASSERT_EQ(j, i++);
    }
}

TYPED_TEST(DequeFixture, test_iterator_add) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x {1, 2, 3};
    iterator b = begin(x);
    iterator e = end(x);
    b += 1;
    ASSERT_EQ(std::distance(b, e), 2);
    ASSERT_EQ(*b, 2);
}

TYPED_TEST(DequeFixture, test_const_iterator) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x {1, 2, 3};
    const_iterator b = begin(x);
    const_iterator e = end(x);
    EXPECT_TRUE(b != e);
    ASSERT_EQ(*b, 1);
    ++b;
    ASSERT_EQ(*b, 2);
    ++b;
    ASSERT_EQ(*b, 3);
    ++b;
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test_const_iterator_distance) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x {1, 2, 3};
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_EQ(std::distance(b, e), 3);
}

TYPED_TEST(DequeFixture, test_const_iterator_range_for) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x {1, 2, 3};
    int i = 1;
    for (const int& j : x) {
        ASSERT_EQ(j, i++);
    }
}

TYPED_TEST(DequeFixture, test_const_iterator_add) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x {1, 2, 3};
    const_iterator b = begin(x);
    const_iterator e = end(x);
    b += 1;
    ASSERT_EQ(std::distance(b, e), 2);
    ASSERT_EQ(*b, 2);
}

TYPED_TEST(DequeFixture, test_clear1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x {1, 2, 3};
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), 3u);
    x.clear();
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0);
}

TYPED_TEST(DequeFixture, test_clear2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0);
    x.clear();
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0);
}

TYPED_TEST(DequeFixture, test_empty) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    ASSERT_TRUE(x.empty());
}

TYPED_TEST(DequeFixture, test_erase1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x{1, 2, 3};
    ASSERT_EQ(x.size(), 3);
    x.erase(begin(x));
    ASSERT_EQ(x.size(), 2);
    ASSERT_EQ(x[0], 2);
    ASSERT_EQ(x[1], 3);
}

TYPED_TEST(DequeFixture, test_erase2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x{1, 2, 3};
    ASSERT_EQ(x.size(), 3);
    x.erase(--end(x));
    ASSERT_EQ(x.size(), 2);
    ASSERT_EQ(x[0], 1);
    ASSERT_EQ(x[1], 2);
}

TYPED_TEST(DequeFixture, test_erase3) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x{1, 2, 3};
    ASSERT_EQ(x.size(), 3);
    x.erase(begin(x) + 1);
    ASSERT_EQ(x.size(), 2);
    ASSERT_EQ(x[0], 1);
    ASSERT_EQ(x[1], 3);
}

TYPED_TEST(DequeFixture, test_erase4) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x{1, 2, 3};
    ASSERT_EQ(x.size(), 3);
    x.erase(std::find(std::begin(x), std::end(x), 2));
    ASSERT_EQ(x.size(), 2);
    ASSERT_EQ(x[0], 1);
    ASSERT_EQ(x[1], 3);
}

TYPED_TEST(DequeFixture, test_insert1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x{1, 2, 3};
    ASSERT_EQ(x.size(), 3);
    x.insert(begin(x), 5);
    ASSERT_EQ(x.size(), 4);
    ASSERT_EQ(x[0], 5);
    ASSERT_EQ(x.front(), 5);
}

TYPED_TEST(DequeFixture, test_insert2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x{1, 2, 3};
    ASSERT_EQ(x.size(), 3);
    x.insert(end(x), 5);
    ASSERT_EQ(x.size(), 4);
    ASSERT_EQ(x[x.size() - 1], 5);
    ASSERT_EQ(x.back(), 5);
}

TYPED_TEST(DequeFixture, test_insert3) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x{1, 2, 3};
    ASSERT_EQ(x.size(), 3);
    x.insert(begin(x) + 1, 5);
    ASSERT_EQ(x.size(), 4);
    ASSERT_EQ(x[0], 1);
    ASSERT_EQ(x[1], 5);
    ASSERT_EQ(x[2], 2);
}

TYPED_TEST(DequeFixture, test_eq1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x, y;
    ASSERT_EQ(x, y);
}

TYPED_TEST(DequeFixture, test_eq2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x{1, 2, 3};
    deque_type y;
    y.push_back(1);
    y.push_back(2);
    y.push_back(3);
    ASSERT_EQ(x, y);
}

TYPED_TEST(DequeFixture, test_resize1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x{1, 2, 3};
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), 3);
    x.resize(0);
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0);
}

TYPED_TEST(DequeFixture, test_resize2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x{1, 2, 3};
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), 3);
    x.resize(2);
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), 2);
    ASSERT_EQ(x.back(), 2);
}

TYPED_TEST(DequeFixture, test_resize3) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x{1, 2, 3};
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), 3);
    x.resize(5);
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), 5);
    ASSERT_EQ(x.back(), 0);
}

TYPED_TEST(DequeFixture, test_resize4) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0);
    x.resize(3);
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), 3);
    ASSERT_EQ(x.front(), 0);
}

TYPED_TEST(DequeFixture, test_resize_value1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x{1, 2, 3};
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), 3);
    x.resize(0, 0);
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0);
}

TYPED_TEST(DequeFixture, test_resize_value2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x{1, 2, 3};
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), 3);
    x.resize(2, 5);
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), 2);
    ASSERT_EQ(x.back(), 2);
}

TYPED_TEST(DequeFixture, test_resize_value3) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x{1, 2, 3};
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), 3);
    x.resize(5, 5);
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), 5);
    ASSERT_EQ(x.back(), 5);
}

TYPED_TEST(DequeFixture, test_resize_value4) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0);
    x.resize(3, 5);
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), 3);
    ASSERT_EQ(x.front(), 5);
}

TYPED_TEST(DequeFixture, test_swap) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x {1, 2, 3};
    deque_type y {3, 2, 1};
    deque_type q {1, 2, 3};
    deque_type z {3, 2, 1};

    x.swap(y);
    ASSERT_EQ(x, z);
    ASSERT_EQ(y, q);
}


TYPED_TEST(DequeFixture, big_test_1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(1000, 1);
    ASSERT_EQ(x.size(), 1000);
    for (int i = 0; i < (int) x.size(); ++i) {
        ASSERT_EQ(x[i], 1);
    }
}


TYPED_TEST(DequeFixture, big_test_2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for (int i = 0; i < (int) x.size(); ++i) {
        x.push_front(i);
    }
    for (int i = 0; i < (int) x.size(); ++i) {
        ASSERT_EQ(x[i], 1000 - i);
    }
}

TYPED_TEST(DequeFixture, big_test_3) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for (int i = 0; i < (int) x.size(); ++i) {
        x.push_back(i);
    }
    for (int i = 0; i < (int) x.size(); ++i) {
        ASSERT_EQ(x[i], i);
    }
}